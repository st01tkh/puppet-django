# class django::supervisor_configuration
#
# Setuping the supervisor tasks:
#  - gunicorn_workers:
#    for the gunicorn script
#
#  - django_watchdog_gunicorn_reloader:
#    reloading the gunicorn on *.py and *.html change
#
#  - django_watchdog_static_reloader:
#    static file if anything changes in the django application
#    assets folder

class django::supervisor_configuration {
  $path                          = $django::path
  $app_name                      = $django::app_name
  $logs_path                     = "${path}/logs"
  $directory                     = "${path}/${app_name}"
  $collect_static                = $django::collect_static
  $virtualenv_path               = $django::virtualenv_path
  $restart_gunicorn              = $django::restart_gunicorn
  $update_nginx_ownership        = $django::update_nginx_ownership
  $django_virtualenv_environment = $django::django_virtualenv_environment


  supervisor::program { 'gunicorn_workers':
    program_user           => 'root',
    program_command        => "${directory}/gunicorn.sh",
    program_directory      => $directory,
    program_autostart      => true,
    program_autorestart    => true,
    program_environment    => $django_virtualenv_environment,
    program_stdout_logfile => "${logs_path}/gunicorn_workers.log",
    program_stderr_logfile => "${logs_path}/gunicorn_workers.error",
  }

  supervisor::program { 'django_watchdog_gunicorn_reloader':
    program_user           => 'root',
    program_command        => "${virtualenv_path}/bin/watchmedo shell-command --patterns='*.py;*.html' --recursive --command='${restart_gunicorn}'",
    program_directory      => $path,
    program_autostart      => true,
    program_autorestart    => true,
    program_environment    => $django_virtualenv_environment,
    program_stdout_logfile => "${logs_path}/django_watchdog_gunicorn_reloader.log",
    program_stderr_logfile => "${logs_path}/django_watchdog_gunicorn_reloader.error",
  }

  supervisor::program { 'django_watchdog_static_collector':
    program_user           => 'root',
    program_command        => "${virtualenv_path}/bin/watchmedo shell-command --patterns='*' --recursive --command='${collect_static}'",
    program_directory      => "${directory}/assets",
    program_autostart      => true,
    program_autorestart    => true,
    program_environment    => $django_virtualenv_environment,
    program_stdout_logfile => "${logs_path}/django_watchdog_static_collector.log",
    program_stderr_logfile => "${logs_path}/django_watchdog_static_collector.error",
  }
}
